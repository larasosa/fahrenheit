package sheridan;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

//import static org.junit.Assert.*;

public class FahrenheitTest {

	
	@Test 
	public void testCorrectTempHappy() {
		boolean temp = Fahrenheit.convertFromCelsius(23);
		assertTrue("that is a correct value for temperature", temp==true);
	}
	@Test 
	public void testCorrectTempSad() {
		boolean temp = Fahrenheit.convertFromCelsius(-12);
		assertTrue("the temperature needsto be a positive value", temp==true);
	}
	@Test 
	public void testCorrectTempIn() {
		boolean temp = Fahrenheit.convertFromCelsius(0);
		assertTrue("temperature is okay", temp==true);
	}
	@Test 
	public void testCorrectTempOut() {
		boolean temp = Fahrenheit.convertFromCelsius(225);
		assertTrue("temperature is outside of the normal climate boudary", temp==true);
	}
		
		
	

}
